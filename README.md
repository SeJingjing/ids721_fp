# Rust-Based Sentiment Analysis Web Service

This project develops a robust Rust-based web service for generating sentiment-relevant text and performing sentiment analysis on input text. The service utilizes the Axum framework and RustBert library, leveraging PyTorch's LibTorch for model inferences. This service is designed to be containerized for Kubernetes deployment and includes an automated CI/CD pipeline for streamlined development and deployment.

## Video Link
[Watch the video here](https://youtu.be/4voIdhXp2YE)

## Group Members
Jingjing Feng,
Qiqi Chen,
Yanyan Xiao,
Zhengqi Wang

## Project Overview

### Requirements Fulfillment

1. **Obtain Open Source ML Model**
   - **Model Selected**: Use the model `distilbert-base-uncased-finetuned-sst-2-english` in `transformers` .
   - **Functionality**: The model is capable of generating text and performing sentiment analysis, providing valuable inferences from input data.

2. **Create Rust Web Service for Model Inferences**
   - **Framework Used**: Actix a powerful and efficient web framework.
   - **Features**:
     - Asynchronous handling of requests.
     - Robust error handling to ensure accurate responses from the model.

3. **Containerize Service and Deploy to Kubernetes**
   - **Containerization**: Dockerized the Rust web service.
      Create the dockerfile
   ```bash
         # Use the official Rust image for the build stage
    FROM rust:bookworm as builder
    
    # Create a new empty shell project
    RUN USER=root cargo new --bin final_web
    WORKDIR /final_web
    
    # Copy over your manifests
    COPY ./Cargo.toml ./Cargo.toml
    COPY ./Cargo.lock ./Cargo.lock
    
    # Cache your dependencies (this is a trick to avoid rebuilding dependencies if unchanged)
    RUN cargo build --release
    RUN rm src/*.rs
    
    # Copy your source tree
    COPY ./src ./src
    
    # Build for release
    RUN rm ./target/release/deps/final_web*
    RUN cargo build --release
    
    # Use Debian Bullseye for the runtime base image
    FROM debian:bookworm-slim
    
    # Install necessary packages for ONNX and Actix runtime
    RUN apt-get update && apt-get install -y \
        ca-certificates \
        libssl-dev \
        libc6-dev \
        libgomp1 \
        && rm -rf /var/lib/apt/lists/*
    
    # Install ONNX Runtime
    RUN apt-get update && apt-get install -y wget
    RUN wget https://github.com/microsoft/onnxruntime/releases/download/v1.8.1/onnxruntime-linux-x64-1.8.1.tgz
    RUN tar -zxvf onnxruntime-linux-x64-1.8.1.tgz -C /usr/local/lib
    RUN echo "/usr/local/lib/onnxruntime-linux-x64-1.8.1/lib" > /etc/ld.so.conf.d/onnxruntime.conf
    RUN ldconfig
    
    # Copy the build artifact from the build stage
    COPY --from=builder /final_web/target/release/final_web .
    
    # Copy model file
    COPY ./torch-model.onnx ./
    
    # Set the CMD to your server
    CMD ["./final_web"]
    ```

   
   - **Kubernetes Deployment**:
     - Deployment configurations ensure scalability and reliability.
     - Service tested in a minikube environment for local Kubernetes emulation.

4. **Implement CI/CD Pipeline**
   - **Tools Used**: GitHub Actions for CI/CD.
   - **Pipeline Features**:
     - Automated testing and building of the Docker image.
     - Automated deployment to Kubernetes.

## Setup and Deployment Instructions

### Prerequisites

- Rust (latest stable version)
- Docker
- Kubernetes cluster or Minikube
- Access to a command line/terminal

### Local Development

1. **Clone the Repository**
   ```bash
   git clone https://your-repository-url.com
   cd your-project-directory
   ```

2. **Build the Project**
   ```bash
   cargo build --release
   ```

3. Run the Project Locally
   ```bash
   cargo run
   ```


### Containerization with Docker

1. **Build the Docker Image**
```bash
docker build -t sentiment-analysis-service .
```

2. **Run the Docker Container Locally**
-  Clone the Repository
-  CD into the Source Code
-  Run directly

```bash
docker run -p 8080:8080 sentiment-analysis-service
```

### Deployment to Dockerhub
```
docker build -t <your-image-name>:<tag> .
docker tag <your-image-name>:tag <your-docker-username>/<your-docker-image>:<tag>
docker push <your-docker-username>/<your-docker-image>:<tag>
```


![DickerHub](./Images/docker_hub.jpg)

### Deployment to Kubernetes
1. **Apply Kubernetes Configurations**
```bash
kubectl apply -f kubernetes/
```
![kubectl](./Images/kubectl.png)

2. **Verify Deployment**
```bash
kubectl get pods
```
![kubectl_deployment](./Images/kubectl_deployment.png)

![Kubernetes_Engine](./Images/Kubernetes_Engine.png)

![kubectl_get_services](./Images/kubectl_get_services.png)

3. **Monitor View**
![kubectl_monitor](./Images/kubectl_monitor.png)

![Monitor](./Images/Metrics.jpg)
![Monitor2](./Images/Metrics2.jpg)
![Monitor3](./Images/Metrics3.jpg)

### CI/CD Pipeline
1. The CI/CD pipeline is configured in .github/workflows/ci-cd.yaml.
2. Changes pushed to the main branch trigger the pipeline automatically.
```bash
stages:
  - build
  - docker
  - deploy

run-python:
  stage: build
  image: python:3.9
  before_script:
    - pip install transformers torch
    - pip3 install onnx
  script:
    - python main.py
  artifacts:
    paths:
      - torch-model.onnx
    expire_in: 1 hour

build:
  stage: docker
  image: docker:latest
  services:
    - docker:dind
  script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin
    - docker build -t jingjing26/ids721fp:latest .
    - docker push jingjing26/ids721fp:latest

deploy:
  stage: deploy
  image: google/cloud-sdk:latest
  script:
    - echo $GKE_SERVICE_KEY > gcloud-service-key.json
    - gcloud auth activate-service-account --key-file $GKE_SERVICE_KEY
    - gcloud container clusters get-credentials ids721fp --zone us-central1 --project pivotal-expanse-421601
    - kubectl apply -f k8s.yaml
    - kubectl rollout restart deployment ids721fp-deployment
    - kubectl get deployments
    - kubectl get pods -l app=ids721fp
    - kubectl get svc
  only:
    - main
```

The CI/CD has been successfully deployed. 
![CI/CD](./Images/CI_CD.png)

### Testing the Service
Use Postman or curl to send requests to the deployed service.

1. Localtest
```bash
curl -X POST http://localhost:8080/analyze -d '{"text":"Your sample text here"}'
```

2. Website link
http://34.71.243.160/
if you need to use this port for testing, you need to add the test message as:
text='text message"

For example:
```angular2html
http://34.71.243.160/?text=happy
```